FROM registry.gitlab.com/docker146/php8-fpm-composer

RUN apk add --no-cache \
					supervisor \
					nginx

COPY supervisord.conf /etc/
COPY supervisord.conf /etc/supervisor/

COPY nginx.conf /etc/nginx/

COPY php.ini /usr/local/etc/php/
COPY php-fpm.conf /usr/local/etc/

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

COPY --chown=www-data:www-data index.php /var/www/html/

RUN chown -R www-data:www-data /var/lib/nginx

EXPOSE 80

CMD ["/usr/bin/supervisord"]
